@extends('layouts.app')

@section('content')



<h1 style="text-align: center">Add company</h1>

        <form method="POST" action="{{ action("CompaniesController@store") }}">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" class="form-control" id="">
            </div>
            
            <div class="form-group">
                <label for="code">Code</label>
                <input type="number" name="code" id="code" class="form-control" id="">
            </div>

            <div class="form-group">
                <label for="address">Address</label>
                <input type="text" name="address" id="address" class="form-control" id="">
            </div>

            <div class="form-group">
                <label for="city">City</label>
                <input type="text" name="city" id="city" class="form-control" id="">
            </div>
            
            <div class="form-group">
                <label for="country">Country</label>
                <input type="text" name="country" id="country" class="form-control" id="">
            </div>
          
            

            <input type="submit" class="btn btn-primary" value="submit">
            @csrf
        </form>
    
        
@endsection