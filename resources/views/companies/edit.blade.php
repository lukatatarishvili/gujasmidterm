@extends('layouts.app')

@section('content')



<h1 style="text-align: center">Edit company</h1>

        <form method="POST" action="{{ action("CompaniesController@update", $companies->id) }}" >
        
           
            
            
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" name="name" id="name" class="form-control"  value="{{$companies->name}}">
            </div>
            
            <div class="form-group">
                <label for="code">Code</label>
                <input type="number" name="code" id="code" class="form-control"  value="{{$companies->code}}">
            </div>

            <div class="form-group">
                <label for="address">Address</label>
                <input type="text" name="address" id="address" class="form-control"  value="{{$companies->address}}">
            </div>

            <div class="form-group">
                <label for="city">City</label>
                <input type="text" name="city" id="city" class="form-control"  value="{{$companies->city}}">
            </div>
            
            <div class="form-group">
                <label for="country">Country</label>
                <input type="text" name="country" id="country" class="form-control"  value="{{$companies->country}}">
            </div>
            

            <input type="submit" class="btn btn-primary" value="submit">
            @method('PUT')
            @csrf
        </form>
    
        
@endsection