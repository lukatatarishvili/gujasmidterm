@extends('layouts.app')

@section('content')
<h1 style="text-align: center">Companies</h1>
@if (count($companies)>0)
<a href="companies/create" class="btn btn-primary" style="margin-bottom: 15px;">Create company</a>

    <div class="card">
      
            
        
                
        

        <table id="companies-table" class="table table-hover companies-table">
            <thead>
              <tr>
                <th scope="col">#</th>
                
                <th scope="col">name</th>      
                <th scope="col">Code</th>      
                <th scope="col">Address</th>      
                <th scope="col">City</th>      
                <th scope="col">Country</th>      
                <th scope="col">Created_at</th>
                <th scope="col">Action</th>
                
                
              </tr>
            </thead>
            <tbody>
                @foreach ($companies as $company)
                <tr>
                    <td> {{$company->id}}</td>
                    <td>{{$company->name}} </td>
                    <td>{{$company->code}} </td>
                    <td>{{$company->address}} </td>
                    <td>{{$company->city}} </td>
                    <td>{{$company->country}} </td>
                    <td> {{$company->created_at}}</td>
                    <td>
                      <form method="POST" action="{{ action("CompaniesController@destroy", $company->id) }}" >
                        <a href="companies/{{$company->id}}/edit" class="btn btn-primary">Edit</a>
                        <input type="submit" value="Delete" class="btn btn-danger">
                        @method("DELETE")
                        @csrf
                    </form>
                    </td>

                    @csrf
                    
                    
                    </td>
                </tr>
                @endforeach
            </tbody>
          </table>
          @else
          <hr>
          <center>
            
          <h3>There is no data! Plase add some rocords</h3>
          <a href="companies/create" class="btn btn-primary" style="margin-bottom: 15px;">Create company</a>
          </center>
          @endif
          
    </div>
        
    
@endsection
